import { Request, Response, Router } from "express";
import { cfg } from "../cfg";
import { openDB, DB } from "../db";
import { layoutVariables } from "../layout";
import { Person } from "../logic/people.logic";
import { logger } from "../logger";
import { isUsernameValid } from "../helpers/validateUsername";
import {
  usernameAlreadyExistsMessage,
  idDoesNotExistMessage,
} from "../helpers/messages";
import { getPersonByUsername, getPersonById } from "../helpers/person";

export const router_people = Router();
export const usernameError = "Hello from ts";

/**
 * Checks if the username is unique
 * - Returns false if the username exists in the database (not unique)
 * - Returns true if the username does not exist in the database (unique)
 * @param db DB
 * @param username string
 * @returns Promise<boolean>
 */
async function isUsernameUnique(username: string, db: DB): Promise<boolean> {
  const person = await db.get(`SELECT * FROM person WHERE username = ?;`, [
    username,
  ]);
  return person ? false : true;
}

router_people.get("/", async (req: Request, res: Response) => {
  const db = await openDB();

  const people = await db.all<Person[]>(
    `SELECT * FROM person ORDER BY updated_at DESC`
  );

  res.render("people", {
    ...layoutVariables,
    title: `${cfg.title} | People`,
    people,
  });
});

router_people.post("/new", async (req: Request, res: Response) => {
  const db = await openDB();

  try {
    const person = req.body;
    const username = person.username;

    if (!username) {
      throw Error("No username. Username is required.");
    }

    if (!isUsernameValid) {
      throw Error("Username is not valid.");
    }

    const unique = await isUsernameUnique(username, db);
    if (!unique) {
      throw Error(usernameAlreadyExistsMessage(username));
    }

    const runResult = await db.run(
      `
      INSERT INTO person (
        username, name, title, bio, location,
        timezone, time_at_company,
        skills, languages
      )
      VALUES (
        :username, :name, :title, :bio, :location,
        :timezone, :timeAtCompany,
        :skills, :languages
      )
    `,
      {
        ":username": person.username,
        ":name": person.name,
        ":title": person.title,
        ":bio": person.bio,
        ":location": person.location,
        ":timezone": person.timezone,
        ":timeAtCompany": person.timeAtCompany,
        ":skills": person.skills,
        ":languages": person.languages,
      }
    );
    const url = `/people/${runResult.lastID}`;
    res.redirect(url);
  } catch (error: any) {
    logger.info(error.message);
    res.redirect("/people/new");
  }
});

router_people.get("/new", async (req: Request, res: Response) => {
  res.render("set_person", {
    ...layoutVariables,
    title: `${cfg.title} | New Person`,
  });
});

router_people.get("/@:username", async (req: Request, res: Response) => {
  try {
    const username = req.params.username;

    const db = await openDB();
    const person = await getPersonByUsername(username, db);

    if (!person) {
      throw Error(`No person found with username "${username}"`);
    }

    res.render("person", {
      ...layoutVariables,
      title: `${cfg.title} | ${person?.name || "No Person"}`,
      username,
      person,
    });
  } catch (error: any) {
    logger.error(error.message);
    res.redirect("/people");
  }
});

router_people.get("/:person_id", async (req: Request, res: Response) => {
  try {
    const person_id = Number(req.params.person_id);

    if (typeof person_id !== "number" || person_id <= 0) {
      throw new Error("person_id must be an interger greater than 0");
    }

    const db = await openDB();
    const person = await getPersonById(person_id, db);

    if (!person) {
      res.redirect("/people");
      return;
    }

    res.render("person", {
      ...layoutVariables,
      title: `${cfg.title} | ${person?.name || "No Person"}`,
      person_id,
      person,
    });
  } catch (error: any) {
    console.error(error);
    res.status(400).send(error.message);
  }
});

router_people.get("/:person_id/edit", async (req: Request, res: Response) => {
  try {
    const id = Number(req.params.person_id);
    const db = await openDB();

    const person = await getPersonById(id, db);

    if (!person) {
      throw Error(idDoesNotExistMessage(id));
    }

    res.render("set_person", {
      ...layoutVariables,
      title: `${cfg.title} | ${person?.name || "No Person"}`,
      id,
      person,
    });
  } catch (error: any) {
    logger.error(error.message);
    res.redirect("/people");
  }
});

router_people.post("/:person_id/edit", async (req: Request, res: Response) => {
  const db = await openDB();

  try {
    const personId = Number(req.params.person_id);
    const person = req.body;

    const db_person = await getPersonById(personId, db);
    if (!db_person) {
      throw Error(idDoesNotExistMessage(personId));
    }
    const personUsername = person.username;
    const unique = await isUsernameUnique(personUsername, db);
    if (!unique) {
      logger.info(usernameAlreadyExistsMessage(personUsername));
      res.redirect(`/people/${personId}/edit`);
      return;
    }

    await db.run(
      `
      UPDATE person
      SET
          username = :username,
          name = :name,
          title = :title,
          bio = :bio,
          location = :location,
          timezone = :timezone,
          time_at_company = :timeAtCompany,
          skills = :skills,
          languages = :languages
      WHERE id = :id
    `,
      {
        ":username": person.username,
        ":name": person.name,
        ":title": person.title,
        ":bio": person.bio,
        ":location": person.location,
        ":timezone": person.timezone,
        ":timeAtCompany": person.timeAtCompany,
        ":skills": person.skills,
        ":languages": person.languages,
        ":id": personId,
      }
    );

    res.redirect(`/people/${personId}`);
  } catch (error: any) {
    logger.info(error.message);
    res.redirect(`/people`);
  }
});
