import "mocha";
import { default as request } from "supertest";
import { app } from "../app";
import expect from "expect.js"; // Use Chai for assertions
import sinon from "sinon";

describe("People Route", function () {
  let openDBStub: sinon.SinonStub;

  beforeEach(() => {
    openDBStub = sinon.stub(require("../db"), "openDB");
  });

  afterEach(function () {
    openDBStub.restore();
  });

  it("should retrieve a list of people", async function () {
    const mockName = "MOCKNAME MOCKGEE";
    openDBStub.resolves({
      all: sinon.stub().resolves([{ id: 1, name: mockName }]),
    });

    const res = await request(app).get("/people");

    expect(res.text).to.contain(mockName);
  });

  it("should create a new person", async function () {
    openDBStub.resolves({
      run: sinon.stub().resolves({ lastID: 1001 }),
      get: sinon.stub().resolves(undefined),
    });

    const newPersonData = {
      username: "john_doe",
      name: "John Doe",
      title: "Developer",
      bio: "A bio about John Doe",
      location: "New York",
      timezone: "UTC-5",
      timeAtCompany: "2 years",
      skills: "JavaScript, TypeScript",
      languages: "English, Spanish",
    };

    const res = await request(app).post("/people/new").send(newPersonData);
    expect(res.text).to.contain("Redirecting to /people/1001");
  });

  it("should retrieve a person by ID", async function () {
    const personId = 1;

    openDBStub.resolves({
      get: sinon.stub().resolves({
        id: personId,
        name: "John Doe",
      }),
    });

    const res = await request(app).get(`/people/${personId}`);

    expect(res.text).to.contain("John Doe");
  });

  it("should not retrieve a person with an id that does not exist", async function () {
    const personId = 1;

    openDBStub.resolves({
      get: sinon.stub().resolves({
        id: personId,
        name: "John Doe",
      }),
    });

    const res = await request(app).get(`/people/2`);

    expect(res.text).to.contain("/people");
  });

  it("should retrieve a person by username", async function () {
    const username: string = "username";

    openDBStub.resolves({
      get: sinon.stub().resolves({
        username: username,
        name: "John Doe",
      }),
    });

    const res = await request(app).get(`/people/@${username}`);

    expect(res.text).to.contain("John Doe");
  });

  it("should update a person", async function () {
    const originalPerson = {
      id: 1,
      username: "Original_Username",
      name: "Opdated Name",
      title: "Opdated Title",
    };
    const updatedPerson = {
      id: 1,
      username: "Updated_Username",
      name: "Updated Name",
      title: "Updated Title",
      // Add other updated fields here
    };

    openDBStub.resolves({
      run: sinon.stub().resolves(originalPerson),
      get: sinon.stub().resolves(originalPerson),
    });

    const res = await request(app)
      .post(`/people/${originalPerson.id}/edit`)
      .send(updatedPerson);

    expect(res.text).to.contain("Redirecting to /people/1");
  });

  it("should not create a person with a username the same as someone else", async function () {
    const firstPerson = {
      id: 1,
      username: "test_username",
      name: "test_name",
    };

    openDBStub.resolves({
      run: sinon.stub().resolves(firstPerson),
      get: sinon.stub().resolves(firstPerson),
    });

    const secondPerson = structuredClone(firstPerson);
    secondPerson.id = 2;

    const res = await request(app).post("/people/new").send(secondPerson);
    expect(res.text).to.contain("Redirecting to /people/new");
  });

  it("should not update a person with a username the same as someone else", async function () {
    const firstPerson = {
      id: 1,
      username: "username1",
      name: "name1",
    };
    const secondPerson = {
      id: 2,
      username: "username2",
      name: "name2",
    };

    openDBStub.resolves({
      all: sinon.stub().resolves([firstPerson, secondPerson]),
      get: sinon.stub().resolves(firstPerson),
    });

    const updatedSecondPersonData = {
      id: 2,
      username: "username1",
    };

    const res = await request(app)
      .post(`/people/${updatedSecondPersonData.id}/edit`)
      .send(updatedSecondPersonData);

    expect(res.text).to.contain(
      `Redirecting to /people/${updatedSecondPersonData.id}`
    );
  });
});
