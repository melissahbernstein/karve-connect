import "mocha";
import { default as request } from "supertest";
import { app } from "../app";
import expect from "expect.js";

describe("Landing Route", function () {
  it("has the default page with title and image", async function () {
    const res = await request(app).get("/");

    // if (err) return done(err);

    // // Check if the response body contains the title
    expect(res.text).to.contain("<title>KarveConnect</title>");

    // // Check if the response body contains the image source
    expect(res.text).to.contain("images/Karve-logo-horizontal-black.png");
  });
});
