import { DB } from "../db";

export interface Person {
  id: number;
  username: string;
  name: string;
  title?: string;
  bio?: string;
  location?: string;
  timezone?: string;
  time_at_company?: string;
  skills?: string;
  languages?: string;
}

export async function setPeopleTable(db: DB) {
  await db.run(`
    CREATE TABLE IF NOT EXISTS person (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
        updated_at DATETIME DEFAULT CURRENT_TIMESTAMP,
        name TEXT NOT NULL,
        username TEXT NOT NULL UNIQUE,
        title TEXT,
        bio TEXT,
        location TEXT,
        timezone TEXT,
        time_at_company TEXT,
        skills TEXT,
        languages TEXT
    )`);
  // await db.run(`
  // ALTER TABLE person ADD COLUMN username TEXT;
  // `);
  // ALTER TABLE person ADD COLUMN username TEXT NOT NULL;
}
