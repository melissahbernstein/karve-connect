import { Database, open } from "sqlite";
import sqlite3 from "sqlite3";
import { cfg } from "./cfg";
import { setPeopleTable } from "./logic/people.logic";
import { logger } from "./logger";
import { access, copyFile } from "fs/promises";

sqlite3.verbose();

export type DB = Database<sqlite3.Database, sqlite3.Statement>;

let db: DB;

export async function openDB() {
  if (db) {
    return db;
  }

  try {
    await access(cfg.database.filename);
    await copyFile(cfg.database.initial_file, cfg.database.filename);
    logger.info(
      `Initial database copied from ${cfg.database.initial_file} to ${cfg.database.filename}`
    );
  } catch (error) {
    logger.error(error);
    throw error;
  }

  try {
    logger.info(`Opening ${cfg.database.filename}`);
    db = await open({
      filename: cfg.database.filename,
      driver: sqlite3.cached.Database,
    });
    return db;
  } catch (err) {
    logger.error(`Failed to open db ${cfg.database.filename}`);
    throw err;
  }
}

export async function initDB() {
  const db = await openDB();

  await setPeopleTable(db);
}
