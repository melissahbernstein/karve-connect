import { isUsernameValid } from "./validateUsername";
import expect from "expect.js";

describe("should test helper function isUsernameValid", function () {
  it("should reject a username with spaces in the middle", () => {
    const usernameWithSpaces = "first second";
    const isValid = isUsernameValid(usernameWithSpaces);
    expect(isValid).equal(false);
  });

  it("should reject a username with special characters", () => {
    const usernameWithSpecialCharacter = "first%secon&d";
    const isValid = isUsernameValid(usernameWithSpecialCharacter);
    expect(isValid).equal(false);
  });

  it("should accept a username ending with spaces", () => {
    const usernameEndingWithSpaces = "first ";
    const isValid = isUsernameValid(usernameEndingWithSpaces);
    expect(isValid).equal(true);
  });

  it("should accept username starting with spaces", () => {
    const usernameStartingWithSpaces = " first";
    const isValid = isUsernameValid(usernameStartingWithSpaces);
    expect(isValid).equal(true);
  });

  it("should accept a username with capitlization", () => {
    const usernameWithCapitalization = "FirSt";
    const isValid = isUsernameValid(usernameWithCapitalization);
    expect(isValid).equal(true);
  });

  it("should accept a username with a underscore", () => {
    const usernameWithOneUnderline = "first_Second";
    const isValid = isUsernameValid(usernameWithOneUnderline);
    expect(isValid).equal(true);
  });
});
