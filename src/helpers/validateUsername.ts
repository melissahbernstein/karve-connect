/**
 * Checks if a username is valid
 * A username is valid if
 * - upper and lowercase characters
 * - no spaces (exception: leading and trailing spaces are truncated)
 * - no special characters (e.g. !@#$%^&*()+-=[]{}, expect one _ is allowed)
 * @param username string
 * @returns boolean
 */
export function isUsernameValid(username: string): boolean {
  const regex = /^ *\w{5,} *$/;
  return username.match(regex) ? true : false;
}
