/**
 * Returns a message stating that the username already exists
 * - suggests that the user pick a different username
 * @param username string
 * @returns string
 */
export function usernameAlreadyExistsMessage(username: string) {
  return `A user with the username "${username}" already exists. Please pick a different username.`;
}
/**
 * Returns a message stating that the username already exists
 * - suggests that the user pick a different username
 * @param username string
 * @returns string
 */
export function idDoesNotExistMessage(id: number) {
  return `No person found with id "${id}"`;
}
