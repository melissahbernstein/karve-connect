import { DB } from "../db";
import { Person } from "../logic/people.logic";

/**
 * Gets a person from the database by their id
 *
 * @param id number
 * @param db DB
 * @returns
 */
export async function getPersonById(
  id: number,
  db: DB
): Promise<Person | undefined> {
  const person = await db.get<Person>(`SELECT * FROM person WHERE id = ?;`, [
    id,
  ]);
  return person;
}

/**
 * Gets a person from the database by their username
 * @param username string
 * @param db DB
 * @returns
 */
export async function getPersonByUsername(
  username: string,
  db: DB
): Promise<Person | undefined> {
  const person = await db.get<Person>(
    `SELECT * FROM person WHERE username = ?;`,
    [username]
  );
  return person;
}
