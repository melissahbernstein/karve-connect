# Contributing to KarveConnect

Welcome to the KarveConnect community! We appreciate your contributions to our project. To ensure a smooth collaboration, please follow these guidelines when creating merge requests and reviewing code.

## Creating a Merge Request (MR)

### Fork the Repository

1. Fork the [Project Repository](https://gitlab.com/your/project/repository) to your GitLab account.
2. Clone your forked repository to your local machine:
3. Create a new branch for your feature or bug fix `git checkout -b feature-name`
4. Make your changes and commit them: `git add .` & `git commit -m "Description of your changes"`
5. Push your changes to your fork: `git push origin feature-name`

### Create the Merge Request

1. Visit the [Project Repository](https://gitlab.com/jaredkozak/karve-connect/-/merge_requests) on GitLab.
2. Click the "New Merge Request" button.
3. Set the source branch (your fork) and target branch (the main project branch).
4. Use the provided template to describe your changes, provide context, and tag relevant team members.
5. Submit the MR.

## Code Review Guidelines

When reviewing code in merge requests, please consider the following aspects:

- **Code Quality**: Ensure the code follows the project's coding standards and best practices.

- **Functionality**: Test the feature or bug fix for correctness and completeness.

- **Performance**: Check for any potential performance bottlenecks or inefficiencies.

- **Documentation**: Verify that code changes are appropriately documented, including comments, README updates, and inline explanations.

- **Testing**: Ensure the changes are covered by unit tests or integration tests, and that existing tests are not broken.

- **Edge Cases**: Look for edge cases and error handling. Consider different scenarios and input variations.

- **Security**: Be vigilant for security vulnerabilities. Avoid SQL injection, XSS, and other common security issues.

- **Consistency**: Ensure consistent naming conventions, formatting, and coding style.

- **Modularity**: Assess the modularity and maintainability of the code. Avoid overly complex or tightly coupled components.

- **Comments**: Review code comments for clarity and correctness.

- **Code Ownership**: Respect the original author's code ownership and style, but provide constructive feedback for improvement.

- **Collaboration**: Maintain a constructive and collaborative tone in your comments.

By following these guidelines, we can maintain code quality, improve collaboration, and ensure a seamless development process.

Thank you for your contributions and dedication to the KarveConnect project!

## Code of conduct

As contributors and maintainers of this project, we pledge to respect all people
who contribute through reporting issues, posting feature requests, updating
documentation, submitting pull requests or patches, and other activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal appearance,
body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other contributions that are
not aligned to this Code of Conduct. Project maintainers who do not follow the
Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported by emailing Jared.

This Code of Conduct is adapted from the [Contributor Covenant](https://contributor-covenant.org), version 1.1.0,
available at [https://contributor-covenant.org/version/1/1/0/](https://contributor-covenant.org/version/1/1/0/).

## Coding Standards

### General Principles

1. **Consistency**: Maintain consistent code style and formatting throughout the project.

2. **Modularity**: Encourage modular code design to improve maintainability and reusability.

3. **Documentation**: Document code thoroughly using comments, JSDoc for TypeScript, and README files.

4. **Error Handling**: Implement proper error handling to provide clear error messages and graceful degradation.

5. **Security**: Follow security best practices to protect against common vulnerabilities like SQL injection and XSS.

### TypeScript

1. **Type Annotations**: Use TypeScript type annotations for variables, function parameters, and return types to enhance code clarity and type safety.

2. **Interfaces**: Define clear and descriptive interfaces for data structures and objects.

3. **Enums**: Use TypeScript enums for defining constant values or options.

### Node.js

1. **Node.js Version**: v20

2. **Promises and Async/Await**: Prefer Promises and async/await for asynchronous operations instead of callbacks.

3. **ES6 Modules**: Use ES6 module syntax (`import` and `export`) for module management.

4. **Environment Variables**: Store sensitive information or configuration in environment variables using the [config system](/src/cfg.ts)

### Express.js

1. **Middleware**: Organize middleware functions logically and avoid excessive nesting.

2. **Routing**: Use Express.js Router for defining routes and controllers for route logic.

3. **Validation**: Implement request validation using libraries like `express-validator`.

4. **Error Handling Middleware**: Create a central error handling middleware for consistent error responses.

5. **HTTP Verbs**: Use appropriate HTTP verbs (GET, POST, PUT, DELETE) for CRUD operations.

### Pug (Jade)

1. **Indentation**: Use consistent indentation (preferably 4 spaces) for Pug templates.

2. **Reusability**: Encapsulate reusable components in separate Pug files and include them where needed.

3. **Variables**: Pass data and variables to Pug templates using the `res.render` method.

4. **Conditional Rendering**: Use Pug's conditionals (`if`, `else if`, `else`) for conditional rendering.

### SQLite Database

1. **Prepared Statements**: Use prepared statements and parameterized queries to prevent SQL injection.

2. **Database Connections**: Open and close database connections as needed to avoid resource leaks.

3. **Indexes**: Create appropriate indexes on frequently queried columns for better query performance.

4. **Transactions**: Use transactions when performing multiple database operations to ensure data consistency.

5. **Database Schema**: Maintain a well-documented database schema to track table structures and relationships.

## Code Reviews

1. Conduct code reviews to ensure code quality, adherence to standards, and knowledge sharing among team members.

2. Use code review checklists to guide the review process, covering aspects like functionality, security, and maintainability.

3. Maintain a collaborative and constructive tone in code review comments.
